<?php

declare(strict_types=1);

namespace App\Traits;

use App\Resource\Enum\ReprocessSkill;
use App\Resource\Enum\ResourceClass;
use Doctrine\DBAL\Connection;

trait ReprocessableTrait
{
    private function addResourceItem(
        string $name,
        string $family,
        ResourceClass $resourceClass,
        ReprocessSkill $reprocessSkill,
        bool $isRare,
        string $type,
        array $results
    ): void {
        $connection = $this->getConnection();

        $stmt = $connection->prepare('INSERT INTO reprocessable (name, family, resource_class, reprocess_skill, is_rare, type) VALUES
            (:name, :family, :resourceClass, :reprocessSkill, :isRare, :type)');
        $stmt->bindValue('name', $name);
        $stmt->bindValue('family', $family);
        $stmt->bindValue('resourceClass', $resourceClass->value);
        $stmt->bindValue('reprocessSkill', $reprocessSkill->value);
        $stmt->bindValue('isRare', (int)$isRare);
        $stmt->bindValue('type', $type);
        $stmt->executeStatement();

        $stmt = $connection->prepare('SELECT id FROM reprocessable WHERE name = :name');
        $stmt->bindValue('name', $name);
        $id = (int)$stmt->executeQuery()->fetchOne();

        foreach ($results as $material => $amount) {
            $this->addReprocessResult($id, $material, $amount);
        }
    }

    private function addReprocessResult(int $reprocessableId, string $name, int $amount): void
    {
        $connection = $this->getConnection();

        $stmt = $connection->prepare('SELECT id FROM material WHERE name = :name');
        $stmt->bindValue('name', $name);
        $materialId = $stmt->executeQuery()->fetchOne();

        $this->addSql('INSERT INTO reprocess_result (material_id, amount, item) VALUES 
            (' . $materialId . ', ' . $amount . ', ' . $reprocessableId . ')');
    }

    abstract private function getConnection(): Connection;
}