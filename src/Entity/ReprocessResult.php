<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Table(name="reprocess_result")
 * @ORM\Entity()
 */
class ReprocessResult
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Material")
     * @JoinTable(name="reprocess_result_material", joinColumns={@JoinColumn(name="material_id", referencedColumnName="id")})
     */
    private Material $material;

    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Reprocessable", inversedBy="reprocessResults")
     * @ORM\JoinColumn(name="item", referencedColumnName="id")
     */
    private Reprocessable $item;

    public function getId(): int
    {
        return $this->id;
    }

    public function getMaterial(): Material
    {
        return $this->material;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}