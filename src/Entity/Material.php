<?php

declare(strict_types=1);

namespace App\Entity;

use App\Resource\Enum\Material as MaterialType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="material")
 * @ORM\Entity()
 */
class Material
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length="150")
     */
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): MaterialType
    {
        return MaterialType::from($this->name);
    }
}