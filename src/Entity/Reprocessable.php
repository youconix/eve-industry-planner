<?php

declare(strict_types=1);

namespace App\Entity;

use App\Resource\Enum\ReprocessSkill;
use App\Resource\Enum\ResourceClass;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="reprocessable")
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *   "Ice" = "Ice",
 *   "Moon Ore" = "MoonOre",
 *   "Standard Ore" = "StandardOre"
 * })
 */
abstract class Reprocessable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected int $id;

    /**
     * @ORM\Column(type="string", length="150")
     */
    protected string $name;

    /**
     * @ORM\Column(type="string", length="150")
     */
    protected string $family;

    /**
     * @ORM\Column(type="string", length="150")
     */
    protected string $resourceClass;

    /**
     * @ORM\Column(type="string", length="150")
     */
    protected string $reprocessSkill;

    /**
     * @ORM\Column(type="boolean")
     */
    protected bool $isRare;

    /**
     * @var Collection<ReprocessResult>
     * @ORM\OneToMany(targetEntity="App\Entity\ReprocessResult", mappedBy="item")
     */
    protected Collection $reprocessResults;

    public function getId(): int
    {
        return $this->id;
    }

    public function getItemsRequired(): int
    {
        return 100;
    }

    public function isStandardVariant(): bool
    {
        return $this->getName() === $this->getFamily();
    }

    public function isRare(): bool
    {
        return $this->isRare;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFamily(): string
    {
        return $this->family;
    }

    public function getResourceClass(): ResourceClass
    {
        return ResourceClass::from($this->resourceClass);
    }

    public function getReprocessSkill(): ReprocessSkill
    {
        return ReprocessSkill::from($this->reprocessSkill);
    }

    /**
     * @return Collection<ReprocessResult>
     */
    public function getReprocessResults(): Collection
    {
        return $this->reprocessResults;
    }

}