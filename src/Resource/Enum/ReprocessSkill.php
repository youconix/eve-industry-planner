<?php

declare(strict_types=1);

namespace App\Resource\Enum;

enum ReprocessSkill: string
{
    case ABYSSAL_ORE = 'Abyssal Ore Reprocessing';
    case COHERENT_ORE = 'Coherent Ore Reprocessing';
    case COMMON_MOON_ORE = 'Common Moon Ore Reprocessing';
    case COMPLEX_ORE = 'Complex Ore Reprocessing';
    case EXCEPTIONAL_MOON_ORE = 'Exceptional Moon Ore';
    case ICE = 'Ice Reprocessing';
    case MERCOXIT_ORE = 'Mercoxit Ore Reprocessing';
    case RARE_MOON_ORE = 'Rare Moon Ore Reprocessing';
    case SIMPLE_ORE = 'Simple Ore Reprocessing';
    case UBIQUITOUS_MOON_ORE = 'Ubiquitous Moon Ore Reprocessing';
    case UNCOMMON_MOON_ORE = 'Uncommon Moon Ore Reprocessing';
    case VARIEGATED_ORE = 'Variegated Ore Reprocessing';
}
