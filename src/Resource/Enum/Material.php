<?php

declare(strict_types=1);

namespace App\Resource\Enum;

use App\Resource\Stockable;

enum Material: string implements Stockable
{
    case ATMOSPHERIC_GASES = 'Atmospheric Gases';
    case CADMIUM = 'Cadmium';
    case COBALT = 'Cobalt';
    case CAESIUM = 'Caesium';
    case CHROMIUM = 'Chromium';
    case DYSPROSIUM = 'Dysprosium';
    case EVAPORITE_DEPOSITES = 'Evaporite deposites';
    case HAFNIUM = 'Hafnium';
    case HEAVY_WATER = 'Heavy Water';
    case HELIUM_ISOTOPES = 'Helium Isotopes';
    case HYDROCARBONS = 'Hydrocarbons';
    case HYDROGEN_ISOTOPES = 'Hydrogen Isotopes';
    case ISOGEN = 'Isogen';
    case LIQUID_OZONE = 'Liquid Ozone';
    case MEGACYTE = 'Megacyte';
    case MERCURY = 'Mercury';
    case MEXALLON = 'Mexallon';
    case MORPHITE = 'Morphite';
    case NEODYMIUM = 'Neodymium';
    case NITROGEN_ISOTOPES = 'Nitrogen Isotopes';
    case NOCXIUM = 'Nocxium';
    case OXYGEN_ISOTOPES = 'Oxygen Isotopes';
    case PLATINUM = 'Platinum';
    case PROMETHIUM = 'Promethium';
    case PYERITE = 'Pyerite';
    case SCANDIUM = 'Scandium';
    case SILICATES = 'Silicates';
    case STRONTIUM_CLATHRATES = 'Strontium Clathrates';
    case TECHNETIUM = 'Technetium';
    case THULIUM = 'Thulium';
    case TITANIUM = 'Titanium';
    case TUNGSTEN = 'Tungsten';
    case TRITANIUM = 'Tritanium';
    case VANADIUM = 'Vanadium';
    case ZYDRINE = 'Zydrine';
}