<?php

declare(strict_types=1);

namespace App\Resource\Enum;

enum ResourceClass : string
{
    case STANDARD_ORES = 'Standard ores';
    case ABYSSAL_ORES = 'Abyssal ores';
    case COMMON_MOON_ORES = 'Common Moon Ores';
    case EXCEPTIONAL_MOON_ORES = 'Exceptional Moon Ores';
    case ICE = 'Ice';
    case RARE_MOON_ORES = 'Rare Moon Ores';
    case UBIQUITOUS_MOON_ORES = 'Ubiquitous Moon Ores';
    case UNCOMMON_MOON_ORES = 'Uncommon Moon Ores';
}
