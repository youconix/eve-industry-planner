<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221010122110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Adding materials';
    }

    public function up(Schema $schema): void
    {
        $this->addItem('Atmospheric Gases');
        $this->addItem('Cadmium');
        $this->addItem('Cobalt');
        $this->addItem('Caesium');
        $this->addItem('Chromium');
        $this->addItem('Dysprosium');
        $this->addItem('Evaporite deposites');
        $this->addItem('Hafnium');
        $this->addItem('Heavy Water');
        $this->addItem('Helium Isotopes');
        $this->addItem('Hydrocarbons');
        $this->addItem('Hydrogen Isotopes');
        $this->addItem('Isogen');
        $this->addItem('Liquid Ozone');
        $this->addItem('Megacyte');
        $this->addItem('Mercury');
        $this->addItem('Mexallon');
        $this->addItem('Morphite');
        $this->addItem('Neodymium');
        $this->addItem('Nitrogen Isotopes');
        $this->addItem('Nocxium');
        $this->addItem('Oxygen Isotopes');
        $this->addItem('Platinum');
        $this->addItem('Promethium');
        $this->addItem('Pyerite');
        $this->addItem('Scandium');
        $this->addItem('Silicates');
        $this->addItem('Strontium Clathrates');
        $this->addItem('Technetium');
        $this->addItem('Thulium');
        $this->addItem('Titanium');
        $this->addItem('Tungsten');
        $this->addItem('Tritanium');
        $this->addItem('Vanadium');
        $this->addItem('Zydrine');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM material');
    }

    private function addItem(string $name): void
    {
        $this->addSql('INSERT INTO material (name) VALUES ("' . $name . '")');
    }
}
