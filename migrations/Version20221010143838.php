<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Resource\Enum\Material;
use App\Resource\Enum\ReprocessSkill;
use App\Resource\Enum\ResourceClass;
use App\Traits\ReprocessableTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221010143838 extends AbstractMigration
{
    use ReprocessableTrait;

    private $type = 'Moon Ore';

    public function getDescription(): string
    {
        return 'Adding moon ore';
    }

    public function up(Schema $schema): void
    {
        $this->addBitumens('Butumens', [6000, 400, 65]);
        $this->addBitumens('Brimful Butumens', [6900, 460, 75]);
        $this->addBitumens('Glistening Butumens', [12000, 800, 130], true);

        $this->addCarnotite('Carnotite', [15, 10, 50]);
        $this->addCarnotite('Glowing Carnotite', [30, 20, 100]);
        $this->addCarnotite('Replete Carnotite', [17, 12, 58]);

        $this->addChromite('Chromite', [10, 40]);
        $this->addChromite('Lavish Chromite', [12, 46]);
        $this->addChromite('Shimmering Chromite', [20, 80]);

        $this->addCinnabar('Cinnabar', [15, 10, 50]);
        $this->addCinnabar('Glowing Cinnabar', [30, 20, 100]);
        $this->addCinnabar('Replete Cinnabar', [17, 12, 58]);

        $this->addCobaltite('Cobaltite', [40]);
        $this->addCobaltite('Copious Cobaltite', [46]);
        $this->addCobaltite('Twinkling Cobaltite', [80]);

        $this->addCoesite('Coesite', [2000, 400, 65]);
        $this->addCoesite('Brimful Coesite', [2300, 460, 75]);
        $this->addCoesite('Glistening Coesite', [4000, 800, 130]);

        $this->addEuxenite('Euxenite', [40]);
        $this->addEuxenite('Copious Euxenite', [46]);
        $this->addEuxenite('Twinkling Euxenite', [80]);

        $this->addLoparite('Loparite', [20, 20, 10, 22]);
        $this->addLoparite('Loparite', [23, 23, 12, 25]);
        $this->addLoparite('Shining Loparite', [40, 40, 20, 44]);

        $this->addMonazite('Monazite', [20, 20, 10, 22]);
        $this->addMonazite('Bountiful Monazite', [23, 23, 12, 25]);
        $this->addMonazite('Shining Monazite', [40, 40, 20, 44]);

        $this->addOtavite('Otavite', [10, 40]);
        $this->addOtavite('Lavish Otavite', [12, 46]);
        $this->addOtavite('Shimmering Otavite', [20, 80]);

        $this->addPollucite('Pollucite', [15, 10, 50]);
        $this->addPollucite('Glowing Pollucite', [30, 20, 100]);
        $this->addPollucite('Replete Pollucite', [17, 12, 58]);

        $this->addScheelite('Scheelite', [40]);
        $this->addScheelite('Copious Scheelite', [46]);
        $this->addScheelite('Twinkling Scheelite', [80]);

        $this->addSperrylite('Sperrylite', [10, 40]);
        $this->addSperrylite('Lavish Sperrylite', [12, 46]);
        $this->addSperrylite('Shimmering Sperrylite', [20, 80]);

        $this->addSylvite('Sylvite', [4000, 400, 65]);
        $this->addSylvite('Brimful Sylvite', [4600, 460, 75]);
        $this->addSylvite('Glistening Sylvite', [8000, 800, 130]);

        $this->addTitanite('Titanite', [40]);
        $this->addTitanite('Copious Titanite', [46]);
        $this->addTitanite('Twinkling Titanite', [80]);

        $this->addVanadinite('Vanadinite', [10, 40]);
        $this->addVanadinite('Lavish Vanadinite', [12, 46]);
        $this->addVanadinite('Shimmering Vanadinite', [20, 80]);

        $this->addXenotime('Xenotime', [20, 20, 10, 22]);
        $this->addXenotime('Bountiful Xenotime', [23, 23, 12, 25]);
        $this->addXenotime('Shining Xenotime', [40, 40, 20, 44]);

        $this->addYtterbite('Ytterbite', [20, 20, 10, 22]);
        $this->addYtterbite('Bountiful Ytterbite', [23, 23, 12, 25]);
        $this->addYtterbite('Shining Ytterbite', [40, 40, 20, 44]);

        $this->addZeolites('Zeolites', [8000, 400, 65]);
        $this->addZeolites('Brimful Zeolites', [9200, 460, 75]);
        $this->addZeolites('Glistening Zeolites', [16000, 800, 130]);

        $this->addZircon('Zircon', [15, 10, 50]);
        $this->addZircon('Glowing Zircon', [30, 20, 100]);
        $this->addZircon('Replete Zircon', [17, 12, 58]);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM reprocessable WHERE type = "' . $this->type . '"');
    }

    private function addBitumens(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::HYDROCARBONS->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Bitumens',
            ResourceClass::UBIQUITOUS_MOON_ORES,
            ReprocessSkill::UBIQUITOUS_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addCarnotite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::ATMOSPHERIC_GASES->value => $amounts[0],
            Material::COBALT->value => $amounts[1],
            Material::TECHNETIUM->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Carnotite',
            ResourceClass::RARE_MOON_ORES,
            ReprocessSkill::RARE_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addChromite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::HYDROCARBONS->value => $amounts[0],
            Material::CHROMIUM->value => $amounts[1],
        ];

        $this->addResourceItem(
            $name,
            'Chromite',
            ResourceClass::UNCOMMON_MOON_ORES,
            ReprocessSkill::UNCOMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addCinnabar(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::EVAPORITE_DEPOSITES->value => $amounts[0],
            Material::TUNGSTEN->value => $amounts[1],
            Material::MERCURY->value => $amounts[1],
        ];

        $this->addResourceItem(
            $name,
            'Cinnabar',
            ResourceClass::RARE_MOON_ORES,
            ReprocessSkill::RARE_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addCobaltite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::COBALT->value => $amounts[0],
        ];

        $this->addResourceItem(
            $name,
            'Cobaltite',
            ResourceClass::COMMON_MOON_ORES,
            ReprocessSkill::COMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addCoesite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::SILICATES->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Coesite',
            ResourceClass::UBIQUITOUS_MOON_ORES,
            ReprocessSkill::UBIQUITOUS_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addEuxenite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::SCANDIUM->value => $amounts[0],
        ];

        $this->addResourceItem(
            $name,
            'Euxenite',
            ResourceClass::COMMON_MOON_ORES,
            ReprocessSkill::COMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addLoparite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::HYDROCARBONS->value => $amounts[0],
            Material::SCANDIUM->value => $amounts[1],
            Material::PLATINUM->value => $amounts[2],
            Material::PROMETHIUM->value => $amounts[3],
        ];

        $this->addResourceItem(
            $name,
            'Loparite',
            ResourceClass::EXCEPTIONAL_MOON_ORES,
            ReprocessSkill::EXCEPTIONAL_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addMonazite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::EVAPORITE_DEPOSITES->value => $amounts[0],
            Material::TUNGSTEN->value => $amounts[1],
            Material::CHROMIUM->value => $amounts[2],
            Material::NEODYMIUM->value => $amounts[3],
        ];

        $this->addResourceItem(
            $name,
            'Monazite',
            ResourceClass::EXCEPTIONAL_MOON_ORES,
            ReprocessSkill::EXCEPTIONAL_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addOtavite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::ATMOSPHERIC_GASES->value => $amounts[0],
            Material::CADMIUM->value => $amounts[1],
        ];

        $this->addResourceItem(
            $name,
            'Otavite',
            ResourceClass::UNCOMMON_MOON_ORES,
            ReprocessSkill::UNCOMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addPollucite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::HYDROCARBONS->value => $amounts[0],
            Material::SCANDIUM->value => $amounts[1],
            Material::CAESIUM->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Pollucite',
            ResourceClass::RARE_MOON_ORES,
            ReprocessSkill::RARE_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addScheelite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TUNGSTEN->value => $amounts[0],
        ];

        $this->addResourceItem(
            $name,
            'Scheelite',
            ResourceClass::COMMON_MOON_ORES,
            ReprocessSkill::COMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addSperrylite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::EVAPORITE_DEPOSITES->value => $amounts[0],
            Material::PLATINUM->value => $amounts[1],
        ];

        $this->addResourceItem(
            $name,
            'Sperrylite',
            ResourceClass::UNCOMMON_MOON_ORES,
            ReprocessSkill::UNCOMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addSylvite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::EVAPORITE_DEPOSITES->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Sylvite',
            ResourceClass::UBIQUITOUS_MOON_ORES,
            ReprocessSkill::UBIQUITOUS_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addTitanite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TITANIUM->value => $amounts[0],
        ];

        $this->addResourceItem(
            $name,
            'Titanite',
            ResourceClass::COMMON_MOON_ORES,
            ReprocessSkill::COMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addVanadinite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::SILICATES->value => $amounts[0],
            Material::VANADIUM->value => $amounts[1],
        ];

        $this->addResourceItem(
            $name,
            'Vanadinite',
            ResourceClass::UNCOMMON_MOON_ORES,
            ReprocessSkill::UNCOMMON_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addXenotime(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::ATMOSPHERIC_GASES->value => $amounts[0],
            Material::COBALT->value => $amounts[1],
            Material::VANADIUM->value => $amounts[2],
            Material::DYSPROSIUM->value => $amounts[3],
        ];

        $this->addResourceItem(
            $name,
            'Xenotime',
            ResourceClass::EXCEPTIONAL_MOON_ORES,
            ReprocessSkill::EXCEPTIONAL_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addYtterbite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::SILICATES->value => $amounts[0],
            Material::TITANIUM->value => $amounts[1],
            Material::CADMIUM->value => $amounts[2],
            Material::THULIUM->value => $amounts[3],
        ];

        $this->addResourceItem(
            $name,
            'Ytterbite',
            ResourceClass::EXCEPTIONAL_MOON_ORES,
            ReprocessSkill::EXCEPTIONAL_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addZeolites(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::ATMOSPHERIC_GASES->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Zeolites',
            ResourceClass::UBIQUITOUS_MOON_ORES,
            ReprocessSkill::UBIQUITOUS_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function addZircon(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::SILICATES->value => $amounts[0],
            Material::TITANIUM->value => $amounts[1],
            Material::HAFNIUM->value => $amounts[2],
        ];

        $this->addResourceItem(
            $name,
            'Zircon',
            ResourceClass::RARE_MOON_ORES,
            ReprocessSkill::RARE_MOON_ORE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function getConnection(): Connection
    {
        return $this->connection;
    }
}
