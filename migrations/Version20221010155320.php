<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Resource\Enum\Material;
use App\Resource\Enum\ReprocessSkill;
use App\Resource\Enum\ResourceClass;
use App\Traits\ReprocessableTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221010155320 extends AbstractMigration
{
    use ReprocessableTrait;

    private $type = 'Ice';

    public function getDescription(): string
    {
        return 'Added ice';
    }

    public function up(Schema $schema): void
    {
        $this->addItem(
            'Blue Ice',
            [
                Material::HEAVY_WATER->value => 69,
                Material::LIQUID_OZONE->value => 35,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::OXYGEN_ISOTOPES->value => 414,
            ]
        );

        $this->addItem(
            'Clear Icicle',
            [
                Material::HEAVY_WATER->value => 69,
                Material::LIQUID_OZONE->value => 35,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::HELIUM_ISOTOPES->value => 414,
            ]
        );

        $this->addItem(
            'Dark glitter',
            [
                Material::HEAVY_WATER->value => 691,
                Material::LIQUID_OZONE->value => 1381,
                Material::STRONTIUM_CLATHRATES->value => 69,
            ]
        );

        $this->addItem(
            'Clear Icicle',
            [
                Material::HEAVY_WATER->value => 69,
                Material::LIQUID_OZONE->value => 35,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::HELIUM_ISOTOPES->value => 414,
            ]
        );

        $this->addItem(
            'Enriched Clear Icicle',
            [
                Material::HEAVY_WATER->value => 104,
                Material::LIQUID_OZONE->value => 55,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::HELIUM_ISOTOPES->value => 483,
            ]
        );

        $this->addItem(
            'Gelidus',
            [
                Material::HEAVY_WATER->value => 345,
                Material::LIQUID_OZONE->value => 691,
                Material::STRONTIUM_CLATHRATES->value => 104,
            ]
        );

        $this->addItem(
            'Glacial Mass',
            [
                Material::HEAVY_WATER->value => 69,
                Material::LIQUID_OZONE->value => 35,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::HYDROGEN_ISOTOPES->value => 414,
            ]
        );

        $this->addItem(
            'Glare Crust',
            [
                Material::HEAVY_WATER->value => 1381,
                Material::LIQUID_OZONE->value => 691,
                Material::STRONTIUM_CLATHRATES->value => 35,
            ]
        );

        $this->addItem(
            'Krystallos',
            [
                Material::HEAVY_WATER->value => 173,
                Material::LIQUID_OZONE->value => 691,
                Material::STRONTIUM_CLATHRATES->value => 173,
            ]
        );

        $this->addItem(
            'Pristine White Glaze',
            [
                Material::HEAVY_WATER->value => 104,
                Material::LIQUID_OZONE->value => 55,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::NITROGEN_ISOTOPES->value => 483,
            ]
        );

        $this->addItem(
            'Smooth Glacial Mass',
            [
                Material::HEAVY_WATER->value => 104,
                Material::LIQUID_OZONE->value => 55,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::HYDROGEN_ISOTOPES->value => 483,
            ]
        );

        $this->addItem(
            'Thick Blue Ice',
            [
                Material::HEAVY_WATER->value => 104,
                Material::LIQUID_OZONE->value => 55,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::OXYGEN_ISOTOPES->value => 483,
            ]
        );

        $this->addItem(
            'White Glaze',
            [
                Material::HEAVY_WATER->value => 69,
                Material::LIQUID_OZONE->value => 35,
                Material::STRONTIUM_CLATHRATES->value => 1,
                Material::NITROGEN_ISOTOPES->value => 414,
            ]
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM reprocessable WHERE type = "' . $this->type . '"');
    }

    private function addItem(
        string $name,
        array $materials,
        bool $isRare = false
    ): void {
        $this->addResourceItem(
            $name,
            $name,
            ResourceClass::ICE,
            ReprocessSkill::ICE,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function getConnection(): Connection
    {
        return $this->connection;
    }
}
