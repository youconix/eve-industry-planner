<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221010122107 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Adding ore tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE material (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reprocess_result (id INT AUTO_INCREMENT NOT NULL, material_id INT DEFAULT NULL, item INT DEFAULT NULL, amount INT NOT NULL, INDEX IDX_CDF90034E308AC6F (material_id), INDEX IDX_CDF900341F1B251E (item), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reprocessable (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, family VARCHAR(150) NOT NULL, resource_class VARCHAR(150) NOT NULL, reprocess_skill VARCHAR(150) NOT NULL, is_rare TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reprocess_result ADD CONSTRAINT FK_CDF90034E308AC6F FOREIGN KEY (material_id) REFERENCES material (id)');
        $this->addSql('ALTER TABLE reprocess_result ADD CONSTRAINT FK_CDF900341F1B251E FOREIGN KEY (item) REFERENCES reprocessable (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE reprocess_result DROP FOREIGN KEY FK_CDF90034E308AC6F');
        $this->addSql('ALTER TABLE reprocess_result DROP FOREIGN KEY FK_CDF900341F1B251E');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE reprocess_result');
        $this->addSql('DROP TABLE reprocessable');
    }
}
