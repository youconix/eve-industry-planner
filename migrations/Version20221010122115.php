<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Resource\Enum\Material;
use App\Resource\Enum\ReprocessSkill;
use App\Resource\Enum\ResourceClass;
use App\Traits\ReprocessableTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221010122115 extends AbstractMigration
{
    use ReprocessableTrait;

    private $type = 'Standard Ore';

    public function getDescription(): string
    {
        return 'Adding standard ores';
    }

    public function up(Schema $schema): void
    {
        $this->addArkonor('Arkonor', [3200, 1200, 120]);
        $this->addArkonor('Crimson Arkonor', [3360, 1260, 126]);
        $this->addArkonor('Flawless Arkonor', [3680, 1380, 138], true);
        $this->addArkonor('Prime Arkonor', [3520, 1320, 132], true);

        $this->addBezdnacine('Bezdnacine', [40000, 4800, 128]);
        $this->addBezdnacine('Absyssal Bezdnacine', [42000, 5040, 134], true);
        $this->addBezdnacine('Hadal Bezdnacine', [44000, 5280, 141], true);

        $this->addBistot('Bistot', [3200, 1200, 160]);
        $this->addBistot('Cubic Bistot', [3680, 1380, 184], true);
        $this->addBistot('Monoclinic Bistot', [3520, 1320, 176]);
        $this->addBistot('Triclinic Bistot', [3520, 1320, 176], true);

        $this->addCrokite('Crokite', [800, 2000, 800]);
        $this->addCrokite('Crystalline Crokite', [880, 2200, 880]);
        $this->addCrokite('Pellucid Crokite', [920, 2300, 920], true);
        $this->addCrokite('Sharp Crokite', [840, 2100, 840]);

        $this->addGneiss('Gneiss', [2000, 1500, 800]);
        $this->addGneiss('Brillant Gneiss', [2300, 1725, 920], true);
        $this->addGneiss('Iridescent Gneiss', [2100, 1575, 840]);
        $this->addGneiss('Prismatic Gneiss', [2200, 1650, 880], true);

        $this->addHedbergite('Hedbergite', [450, 120]);
        $this->addHedbergite('Glazed Hedbergite', [495, 132], true);
        $this->addHedbergite('Lustrous Hedbergite', [518, 138], true);
        $this->addHedbergite('Vitric Hedbergite', [473, 126]);

        $this->addHemorphite('Hemorphite', [240, 90]);
        $this->addHemorphite('Radiant Hemorphite', [264, 99]);
        $this->addHemorphite('Scintillating Hemorphite', [276, 104], true);
        $this->addHemorphite('Vivid Hemorphite', [254, 95]);

        $this->addJaspet('Jaspet', [150, 50]);
        $this->addJaspet('Immaculate Jaspet', [173, 58], true);
        $this->addJaspet('Pristine Jaspet', [165, 55], true);
        $this->addJaspet('Pure Jaspet', [158, 53]);

        $this->addKernite('Kernite', [60, 120]);
        $this->addKernite('Fiery Kernite', [66, 132]);
        $this->addKernite('Lumious Kernite', [63, 126]);
        $this->addKernite('Resplendant Kernite', [69, 138], true);

        $this->addMercoxit('Mercoxit', [140]);
        $this->addMercoxit('Magma Mercoxit', [147]);
        $this->addMercoxit('Vitreous Mercoxit', [154], true);

        $this->addDarkOchre('Dark Ochre', [1360, 1200, 320]);
        $this->addDarkOchre('Jet Ochre', [1564, 1380, 368], true);
        $this->addDarkOchre('Obsidian Ochre', [1496, 1320, 352], true);
        $this->addDarkOchre('Onyx Ochre', [1428, 1260, 336]);

        $this->addOmber('Omber', [90, 75]);
        $this->addOmber('Golden Omber', [99, 83]);
        $this->addOmber('Platinoid Omber', [104, 86], true);
        $this->addOmber('Silvery Omber', [95, 79]);

        $this->addPlagioclase('Plagioclase', [175, 70]);
        $this->addPlagioclase('Azure Plagioclase', [184, 75]);
        $this->addPlagioclase('Rich Plagioclase', [193, 77]);
        $this->addPlagioclase('Sparkling Plagioclase', [201, 81], true);

        $this->addPyroxeres('Pyroxeres', [90, 30]);
        $this->addPyroxeres('Opulent Pyroxeres', [104, 35], true);
        $this->addPyroxeres('Solid Pyroxeres', [95, 32]);
        $this->addPyroxeres('Viscous Pyroxeres', [99, 33]);

        $this->addRakovene('Rakovene', [40000, 3200, 200]);
        $this->addRakovene('Abyssal Rakovene', [42000, 3360, 210]);
        $this->addRakovene('Hadal Rakovene', [44000, 3520, 220]);

        $this->addScordite('Scordite', [150, 90]);
        $this->addScordite('Condensed Scordite', [158, 95]);
        $this->addScordite('Glossy Scordite', [173, 104], true);
        $this->addScordite('Massive Scordite', [165, 99]);

        $this->addSpodumain('Spudomain', [48000, 1000, 160, 80, 40]);
        $this->addSpodumain('Bright Spudomain', [50400, 1050, 168, 80, 40]);
        $this->addSpodumain('Dazzling Spudomain', [55200, 1150, 184, 92, 46], true);
        $this->addSpodumain('Gleaming Spudomain', [52800, 1100, 176, 88, 44]);

        $this->addTalassonite('Talassonite', [40000, 960, 32]);
        $this->addTalassonite('Absyssal Talassonite', [42000, 1008, 34]);
        $this->addTalassonite('Hadal Talassonite', [44000, 1056, 35]);

        $this->addVeldspar('Veldspar', [400]);
        $this->addVeldspar('Dense Veldspar', [440]);
        $this->addVeldspar('Stable Veldspar', [460]);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM reprocessable WHERE type = "' . $this->type . '"');
    }

    private function addArkonor(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::MEGACYTE->value => $amounts[2],
        ];

        $this->addItem($name, 'Arkonor', ReprocessSkill::COMPLEX_ORE, $isRare, $materials);
    }

    private function addBezdnacine(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
            Material::ISOGEN->value => $amounts[1],
            Material::MEGACYTE->value => $amounts[2],
        ];

        $this->addItem(
            $name,
            'Bezdnacine',
            ReprocessSkill::ABYSSAL_ORE,
            $isRare,
            $materials,
            ResourceClass::ABYSSAL_ORES
        );
    }

    private function addBistot(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::ZYDRINE->value => $amounts[2],
        ];

        $this->addItem($name, 'Bistot', ReprocessSkill::COMPLEX_ORE, $isRare, $materials);
    }

    private function addCrokite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::NOCXIUM->value => $amounts[2],
        ];

        $this->addItem($name, 'Crokite', ReprocessSkill::VARIEGATED_ORE, $isRare, $materials);
    }

    private function addGneiss(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
            Material::ISOGEN->value => $amounts[2],
        ];

        $this->addItem($name, 'Gneiss', ReprocessSkill::VARIEGATED_ORE, $isRare, $materials);
    }

    private function addHedbergite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::NOCXIUM->value => $amounts[1],
        ];

        $this->addItem($name, 'Hedbergite', ReprocessSkill::COHERENT_ORE, $isRare, $materials);
    }

    private function addHemorphite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::ISOGEN->value => $amounts[0],
            Material::NOCXIUM->value => $amounts[1],
        ];

        $this->addItem($name, 'Hemorphite', ReprocessSkill::COHERENT_ORE, $isRare, $materials);
    }

    private function addJaspet(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::MEXALLON->value => $amounts[0],
            Material::NOCXIUM->value => $amounts[1],
        ];

        $this->addItem($name, 'Jaspet', ReprocessSkill::COHERENT_ORE, $isRare, $materials);
    }

    private function addKernite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::MEXALLON->value => $amounts[0],
            Material::ISOGEN->value => $amounts[1],
        ];

        $this->addItem($name, 'Kernite', ReprocessSkill::COHERENT_ORE, $isRare, $materials);
    }

    private function addMercoxit(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::MORPHITE->value => $amounts[0],
        ];

        $this->addItem($name, 'Mercoxit', ReprocessSkill::MERCOXIT_ORE, $isRare, $materials);
    }

    private function addDarkOchre(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::MEXALLON->value => $amounts[0],
            Material::ISOGEN->value => $amounts[1],
        ];

        $this->addItem($name, 'Dark Ochre', ReprocessSkill::VARIEGATED_ORE, $isRare, $materials);
    }

    private function addOmber(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::ISOGEN->value => $amounts[1],
        ];

        $this->addItem($name, 'Omber', ReprocessSkill::COHERENT_ORE, $isRare, $materials);
    }

    private function addPlagioclase(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
        ];

        $this->addItem($name, 'Plagioclase', ReprocessSkill::SIMPLE_ORE, $isRare, $materials);
    }

    private function addPyroxeres(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::PYERITE->value => $amounts[0],
            Material::MEXALLON->value => $amounts[1],
        ];

        $this->addItem($name, 'Pyroxeres', ReprocessSkill::SIMPLE_ORE, $isRare, $materials);
    }

    private function addRakovene(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
            Material::ISOGEN->value => $amounts[1],
            Material::ZYDRINE->value => $amounts[2],
        ];

        $this->addItem(
            $name,
            'Rakovene',
            ReprocessSkill::ABYSSAL_ORE,
            $isRare,
            $materials,
            ResourceClass::ABYSSAL_ORES
        );
    }

    private function addScordite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
            Material::PYERITE->value => $amounts[1],
        ];

        $this->addItem($name, 'Scordite', ReprocessSkill::SIMPLE_ORE, $isRare, $materials);
    }

    private function addSpodumain(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
            Material::ISOGEN->value => $amounts[1],
            Material::NOCXIUM->value => $amounts[2],
            Material::ZYDRINE->value => $amounts[3],
            Material::MEGACYTE->value => $amounts[4],
        ];

        $this->addItem($name, 'Spodumain', ReprocessSkill::COMPLEX_ORE, $isRare, $materials);
    }

    private function addTalassonite(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
            Material::NOCXIUM->value => $amounts[1],
            Material::MEGACYTE->value => $amounts[2],
        ];

        $this->addItem(
            $name,
            'Talassonite',
            ReprocessSkill::ABYSSAL_ORE,
            $isRare,
            $materials,
            ResourceClass::ABYSSAL_ORES
        );
    }

    private function addVeldspar(string $name, array $amounts, bool $isRare = false): void
    {
        $materials = [
            Material::TRITANIUM->value => $amounts[0],
        ];

        $this->addItem($name, 'Veldspar', ReprocessSkill::SIMPLE_ORE, $isRare, $materials);
    }

    private function addItem(
        string $name,
        string $family,
        ReprocessSkill $reprocessSkill,
        bool $isRare,
        array $materials,
        ResourceClass $resourceClass = null
    ): void {
        $resourceClass = $resourceClass ?? ResourceClass::STANDARD_ORES;

        $this->addResourceItem(
            $name, $family,
            $resourceClass,
            $reprocessSkill,
            $isRare,
            $this->type,
            $materials
        );
    }

    private function getConnection(): Connection
    {
        return $this->connection;
    }
}
